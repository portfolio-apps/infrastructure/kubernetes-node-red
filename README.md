# Kubernetes - Node-RED with import Flows and Creds

## Useful Links
- [Demo](https://node-red.skrumify.com)

### Deploy to Kuberenetes

```bash
kubectl -n $NAMESPACE apply -f .
```

### Add your credentials for flows

Add your secret key in "credentialSecret" in the settings.js file, then add your json from flows_cred.json to creds.yaml.